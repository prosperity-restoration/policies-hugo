---
title: 'About'
menu: 'main'
---

We're making voluntary, localized solutions effective.

## Thanks for Tools & Content

- [Sunflower logo from MediaWiki via Creative Commons](https://search.creativecommons.org/photos/414c7568-336e-4943-8dde-fdad4682d09d)
- [Serif theme](https://themes.gohugo.io/hugo-serif-theme/) from [zerostatic](https://www.zerostatic.io)
- Site builder framework [Hugo](https://gohugo.io/)
- Source code host [GitLab](https://gitlab.com/)

