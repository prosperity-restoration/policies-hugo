---
title: "How to Submit a Policy"
date: 2020-05-26T20:19:00-06:00
featured: true
draft: false
---



In short: create a merge request with a new file into this repository.  It will be merged by our site managers.

1. Open [this link in a separate window](https://gitlab.com/prosperity-restoration/policies-hugo/-/tree/master/content/services).
1. Choose an existing policy or template
  ![](../../images/how-to/policy-1.png "")
1. Choose to display source
  ![](../../images/how-to/policy-2.png "")
1. Copy the file contents
  ![](../../images/how-to/policy-3.png "")
1. Go to the directory where you want to insert the new policy
  ![](../../images/how-to/policy-4.png "")
1. Create a new file
  ![](../../images/how-to/policy-5.png "")
1. Paste the file contents and edit any details
  ![](../../images/how-to/policy-6.png "")
1. Give it a file name, preferably with ".md" on the end
  ![](../../images/how-to/policy-7.png "")
1. At the bottom of the page, write any message, choose "master" branch, and click "Commit Changes"
  ![](../../images/how-to/policy-8.png "")
1. At the top of the page, click "Create merge request"
  ![](../../images/how-to/policy-9.png "")
1. Select from your "/policy-makers:master" to "trentlarson/policy-makers:master", then click "Compare branches and continue"
  ![](../../images/how-to/policy-10.png "")
1. Fill out any descriptive info, then go to the bottom of the page and click "Submit merge request"
  ![](../../images/how-to/policy-11.png "")
1. Success!
  ![](../../images/how-to/policy-12.png "")

Now the managers of this site will review your submission and publish it.  Feel free to raise [an issue](https://gitlab.com/prosperity-restoration/policies-hugo/-/issues) if you need help.

